## Tusk

Tusk is a simple, Evernote controlled site builder.

This is my attempt to:

1. Teach myself some advanced Meteor
2. Get a jump start on the Bootstrap 4 transition from LESS to SCSS
3. Make something useful

## Debug Mode

1. Install node-inspector `npm install -g node-inspector`
2. Start meteor in debug mode `NODE_OPTIONS='--debug' meteor run`
3. Start node-inspector from your project root `node-inspector`
4. Enjoy step-through debugging

If node-inspector, after launching, complains that it cannot find a particular module, then you may have a newer version
of Node running than the Meteor embedded version. In that case, you may need to run:

    npm install node-inspector --target=0.10.36

Where '0.10.36' is the embedded version of Node. Then run node-inspector from the project's node_modules directory.

You can install it globally too, if you so desire:

    sudo npm -g uninstall node-inspector
    sudo npm -g install node-inspector --target=0.10.36