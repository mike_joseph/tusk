if (Meteor.isClient) {

    Template['tusk-index'].events({
        'submit form': function(evt) {
            evt.preventDefault();

            var email = $('[name="email"]').val(),
                pass  = $('[name="password"]').val();

            Meteor.call('users.create', email, pass);

            Meteor.loginWithPassword(email, pass, function() {
                Router.go('/tusk/dash');
            });
        }
    });
}
