TuskController = RouteController.extend({
    layoutTemplate: 'AppLayout',

    onBeforeAction: function() {
        // check route
        // if not /tusk or related
        // call Meteor.userId()
        // if null:
        // Router.go('/');
        this.next();
    }
});


Router.route('/tusk', {
    template: 'tusk-index',
    controller: 'TuskController',
    action: function() {
        this.render('tusk-index');
    }
});