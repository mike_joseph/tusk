module.exports = function(grunt) {
    grunt.initConfig({
        sass: {
            dist: {
                options: {
                    style: 'expanded'
                },
                files: {
                    'app/public/i/master.css': 'assets/scss/master.scss'
                }
            }
        },

        concat: {
            options: {
                banner: '<%= banner %>',
                stripBanners: true
            },
            dist: {
                src: ['assets/js/**/*.js'],
                dest: 'app/public/i/master.js'
            }
        },

        uglify: {
            options: {
                banner: '<%= banner %>'
            },
            dist: {
                src: '<%= concat.dist.dest %>',
                dest: 'app/public/i/master.min.js'
            }
        },

        watch: {
            scss: {
                files: '**/*.scss',
                tasks: ['sass'],
                options: {
                    interrupt: true,
                },
            },
            scripts: {
                files: '**/*.js',
                tasks: ['concat', 'uglify'],
                options: {
                    interrupt: true,
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['sass', 'concat', 'uglify']);

};